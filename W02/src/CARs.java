public class CARs {

    private String make;
    private String model;

    public CARs(String make, String model) {
        this.make = make;
        this.model = model;
    }

    public String toString() {
        return " Make: " + make + " Model: " + model;
    }
}


