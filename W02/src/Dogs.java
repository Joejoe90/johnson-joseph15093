import java.util.*;

class Dogs {

    int size, weight, id;
    String breed;

    public Dogs(int size, int weight, int id,  String breed) {
        this.size = size;
        this.weight = weight;
        this.id = id;
        this.breed = breed;
    }
    public String toString() {
        return this.size + " " + this.weight + " " + this.id + " " + this.breed;
    }
}
class Sortbysize implements Comparator<Dogs> {
    public int compare(Dogs x, Dogs y) {
        return x.size - y.size;
    }
}

class Sortbyweight implements Comparator<Dogs> {
    public int compare(Dogs x, Dogs y) {
        return x.weight - y.weight;
    }
}

class Sortbybreed implements Comparator<Dogs> {
    public int compare(Dogs x, Dogs y) {
        return x.breed.compareTo(y.breed);
    }
}
class Main
{
    public static void main(String[] args) {

        ArrayList<Dogs> ar = new ArrayList<Dogs>();
        ar.add(new Dogs(35, 30, 1, "germanshepherd"));
        ar.add(new Dogs(20, 70, 2, "yorkie"));
        ar.add(new Dogs(40, 100, 3, "xenomorph"));

        System.out.println("-- List --");
        for (int a = 0; a < ar.size(); a++)
            System.out.println(ar.get(a));

        Collections.sort(ar, new Sortbysize());

        System.out.println("\nSorted by weight");
        for (int a = 0; a < ar.size(); a++)
            System.out.println(ar.get(a));

        Collections.sort(ar, new Sortbyweight());

        System.out.println("\nSorted by id");
        for (int a = 0; a < ar.size(); a++)
            System.out.println(ar.get(a));

        System.out.println("-- Set --");
        Set set = new TreeSet();
        set.add("band");
        set.add("skate");
        set.add("zebra");
        set.add("cat");
        set.add("pig");
        set.add("band");
        set.add("zebra");

        for (Object str : set) System.out.println((String) str);
        System.out.println("number of words " + set.size());

        System.out.println("-- Sorted Queue --");
        Queue<String> queue = new PriorityQueue<String>();
        queue.add("band");
        queue.add("skate");
        queue.add("zebra");
        queue.add("cat");
        queue.add("pig");
        queue.add("ape");

        Iterator iterator = queue.iterator();
        while (iterator.hasNext()) System.out.println(queue.poll());

        System.out.println("-- Tree --");
        List<CARs> myList = new LinkedList<CARs>();
        myList.add(new CARs("Dodge", "Charger"));
        myList.add(new CARs("Mazda", "Miata"));
        myList.add(new CARs("Ford", "Mustang"));
        myList.add(new CARs("Dodge", "Challenger"));

        String[] CARs = {"Dodge: " + "Charger", "Mazda: " + "Miata", "Ford: " + "Mustang", "Dodge: " + "Challenger"};
        for (String i : CARs) {
            System.out.println(i);

        }
    }
    }