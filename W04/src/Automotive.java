public class Automotive {

    private String owner;
    private long vin;
    private String make;
    private String model;
    private long year;

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public long getVin() {
        return vin;
    }

    public void setVin(long vin) {
        this.vin = vin;
    }

    public String getMake() { return make; }

    public void setMake(String make) { this.make = make; }

    public String getModel() { return model; }

    public void setModel(String model) { this.model = model; }

    private long getYear() { return year; }

    public void setYear(long year) { this.year = year; }

    public String toString() {
        return "Owner: " + owner + " Vin: " + vin;
    }

}

