
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

class JSONExample {

        public static String automotiveToJSON(Automotive automotive) {

            ObjectMapper mapper = new ObjectMapper();
            String s = "";

            try {
                s = mapper.writeValueAsString(automotive);
            } catch (JsonProcessingException e) {
                System.err.println(e.toString());
            }

            return s;
        }

        public static Automotive JSONToAutomotive(String s) {

            ObjectMapper mapper = new ObjectMapper();
            Automotive automotive = null;

            try {
                automotive = mapper.readValue(s, Automotive.class);
            } catch (JsonProcessingException e) {
                System.err.println(e.toString());
            }

            return automotive;
        }

        public static void main(String[] args) {

            Automotive auto = new Automotive();
            auto.setOwner("Joe");
            auto.setVin(5558885);
            auto.setMake("Ford");
            auto.setModel("Fusion");
            auto.setYear(2010);

            String json = JSONExample.automotiveToJSON(auto);
            System.out.println(json);

            Automotive auto2 = JSONExample.JSONToAutomotive(json);
            System.out.println(auto2);
        }
    }