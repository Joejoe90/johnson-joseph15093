import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutePeopleFishing {

    public static void main(String[] args) {

        ExecutorService myService = Executors.newFixedThreadPool(3);

        PeopleFishing ds1 = new PeopleFishing("Bob", 14, 10);
        PeopleFishing ds2 = new PeopleFishing("Sally", 12, 50);
        PeopleFishing ds3 = new PeopleFishing("Billy", 4, 10);
        PeopleFishing ds4 = new PeopleFishing("Anna", 2, 20);
        PeopleFishing ds5 = new PeopleFishing("Pat", 0, 0);

        myService.execute(ds1);
        myService.execute(ds2);
        myService.execute(ds3);
        myService.execute(ds4);
        myService.execute(ds5);

        myService.shutdown();
    }
}
