import java.util.Random;

public class PeopleFishing implements Runnable {

    private String name;
    private int number;
    private int weight;
    private int rand;

    public PeopleFishing(String name, int number, int weight) {

        this.name = name;
        this.number = number;
        this.weight = weight;

        Random random = new Random();
        this.rand = random.nextInt(1000);
    }

    public void run() {
        System.out.println("\n\nExecuting with these parameters: Name =" + name + " Number = "
                + number + " weight = " + weight + " Rand Num = " + rand + "\n\n");
        for (int count = 1; count < rand; count++) {
            if (count % number == 0) {
                System.out.print(name + " is Fishing. ");
                while(true);
            }
        }
        System.out.println("\n\n" + name + " is done.\n\n");
    }
}